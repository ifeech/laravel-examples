<?php
namespace App\Facades;

use Illuminate\Support\Facades\Facade;

class Sigmasms extends Facade {
    protected static function getFacadeAccessor() { return 'sigmasms'; }
}

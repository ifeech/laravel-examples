<?php

namespace App\Providers;

use App\Services\Sigmasms;
use Illuminate\Support\ServiceProvider;

class SigmasmsServiceProvider extends ServiceProvider
{
    /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
    protected $defer = false;

    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind('sigmasms', function () {
            return new Sigmasms(config('services.sigmasms.url'));
        });
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {

    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function provides()
    {
        return [Sigmasms::class];
    }
}

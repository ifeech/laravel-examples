<?php


namespace App\Services;


use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;
use phpDocumentor\Reflection\Types\Integer;

class Sigmasms
{
    /**
     * @var \GuzzleHttp\Client
     */
    protected $httpClient;
    /**
     * @var string
     */
    protected $token;
    /**
     * @var string
     */
    protected $idUser;
    /**
     * @var string
     */
    private $tokenFilename = 'storage/sigmasmstoken.txt';
    /**
     * @var string
     */
    private $idUserFilename = 'storage/sigmasmsid.txt';
    /**
     * @var string
     */
    private $userName = 'testUser';
    /**
     * @var string
     */
    private $userPass = 'testPassword';

    /**
     * Sigmasms constructor.
     *
     * @param string $url
     *
     * @throws \Exception
     */
    public function __construct($url)
    {
        if (!is_string($url) || empty($url)) {
            throw new \Exception('No correct URL');
        }

        $this->httpClient = new Client(['base_uri' => $url]);
        $this->checkToken();
    }

    /**
     * @param array $scope
     * @return object
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function getUser($scope = [])
    {
        if (!is_array($scope)) {
            $scope = [$scope];
        }

        $params = [
            'headers' => ['Content-Type' => 'application/json', 'Authorization' => $this->token],
            'query' => ['$scope[]'=>$scope]
        ];

        $data = $this->decodeResponseBody($this->api('users/' . $this->idUser, $params));

        return $data;
    }

    /**
     * @param string $phone
     * @param string $sender
     * @param string $text
     * @return object
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function sendOneSMS($phone = '', $sender = '', $text = ''){
         $body = [
            'recipient' => $this->clearPhone($phone),
            'type' => 'sms',
            'payload' => ['sender'=>$sender, 'text'=>$text]
        ];

        $params = [
            'headers' => ['Content-Type' => 'application/json', 'Authorization' => $this->token],
            'body' => json_encode($body)
        ];

        $data = $this->decodeResponseBody($this->api('sendings', $params, 'POST'));

        return $data;
    }

    /**
     * Using CURL to issue a request
     *
     * @param string $url
     * @param array $params
     * @param string $method
     * @return object
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    private function api($url, $params, $method = 'GET')
    {
        try {
            $response = $this->httpClient->request($method, $url, $params);
        } catch (RequestException $exception) {
            throw new \Exception($exception);
        }

        return $response->getBody();
    }

    /**
     * @return string
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    private function getToken()
    {
        $params = [
            'headers' => ['Content-Type' => 'application/json'],
            'body' => json_encode(array('username' => $this->userName, 'password' => $this->userPass))
        ];

        $data = $this->decodeResponseBody($this->api('login', $params, 'POST'));

        file_put_contents($this->tokenFilename, $data->token);
        file_put_contents($this->idUserFilename, $data->id);

        $this->idUser = $data->id;

        return $data->token;
    }

    private function checkToken()
    {
        // проверяем токен
        if (file_exists($this->tokenFilename) && (date('Y-m-d H:i:s', filemtime($this->tokenFilename)) > date('Y-m-d H:i:s', strtotime('-23 hours')))) {
            $this->token = file_get_contents($this->tokenFilename, true);
            $this->idUser = file_get_contents($this->idUserFilename, true);
        } else {
            $this->token = $this->getToken();
        }
    }

    /**
     * Decode the response from Sigmasms
     *
     * @param string $body the api response from Sigmasms
     * @return object
     */
    private function decodeResponseBody($body)
    {
        return json_decode($body);
    }


    /**
     * @param $phone
     * @return string|string[]|null
     */
    function clearPhone($phone) {
        return preg_replace('/[() -]+/', '', $phone);
    }
}

<?php

namespace App\Http\Controllers;

use App\Facades\Sigmasms;
use App\Notifications\UserNotification;
use App\User;

class TestController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function index()
    {
        //получение пользователя sigma sms
        $sigmasmsUser = Sigmasms::getUser(['permissions', 'settings']);

        //отправка тестового сообщения в telegram
        $user = new User();
        $user->notify(new UserNotification());
        dd($sigmasmsUser);
    }
}
